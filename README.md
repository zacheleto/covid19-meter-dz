# COVID19 Meter DZ

A simple COVID19 Meter for Algeria cases app that fetches from an API which can display today's and yesterday's data. 

### Prerequisites

none

### Installing

git clone https://github.com/letowebdev/COVID19-meter-DZ.git

## Deployment

https://zacheleto.me/Projects/covid19dz/

## Built With

* [JavaScript ES6 & ES7]
* [disease.sh API]

## Author

* **Zache Abdelatif (Leto)**

## License

This project is licensed under the MIT License
