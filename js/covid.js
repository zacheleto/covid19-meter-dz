class Covid{

async getData()
{
    // Adding API urls
    const today = 'https://disease.sh/v2/countries/Algeria';
    const yesterday = 'https://disease.sh/v2/countries/Algeria?yesterday=true';
    
    const  todayResponse= await fetch(today);
    const todayData = await todayResponse.json();

    const yesterdayResponse = await fetch(yesterday);
    const yesterdayData = await yesterdayResponse.json();

    return {
        todayData,
        yesterdayData
    }
}









}