// init classes
const covid = new Covid;
const ui = new UI;

//get data
covid.getData()
.then(data => {
    
    //show todays Data
    ui.showTodayData(data.todayData);

    //show yesterdays Data
    ui.showYesterdayData(data.yesterdayData)
})
