class UI{
constructor()
{
    this.getTodayData = document.getElementById('div1');
    this.getYesterdayData = document.getElementById('div2');
}

// display data in UI
// display todays data
showTodayData(todayData)
{
    this.getTodayData.innerHTML = `
    <div class="col-md-12 card card-body text-dark font-weight-bold bg-info">
    <span>Today cases: ${Intl.NumberFormat().format(todayData.todayCases)}</span>
    </div>
    <div class="col-md-12 card card-body text-white bg-danger font-weight-bold">
    <span>Today deaths: ${Intl.NumberFormat().format(todayData.todayDeaths)}</span>
    </div>
    <div class="col-md-12 card card-body text-dark font-weight-bold bg-info">
    <span>Total cases: ${Intl.NumberFormat().format(todayData.cases)}</span>
    </div>
    <div class="col-md-12 card card-body text-dark bg-success font-weight-bold">
    <span>Recovered: ${Intl.NumberFormat().format(todayData.recovered)}</span>
    </div>
    <div class="col-md-12 card card-body text-dark font-weight-bold bg-info">
    <span>Critical cases: ${Intl.NumberFormat().format(todayData.critical)}</span>
    </div>
    <div class="col-md-12 card card-body text-white bg-danger font-weight-bold">
    <span>Total Deaths: ${Intl.NumberFormat().format(todayData.deaths)}</span>
    </div>
        `
}

//display yesterdays data
showYesterdayData(yesterdayData)
{
    this.getYesterdayData.innerHTML = `
    <div class="col-md-12 card card-body text-dark font-weight-bold bg-info">
    <span>Today cases: ${Intl.NumberFormat().format(yesterdayData.todayCases)}</span>
    </div>
    <div class="col-md-12 card card-body text-white bg-danger font-weight-bold">
    <span>Today deaths: ${Intl.NumberFormat().format(yesterdayData.todayDeaths)}</span>
    </div>
    <div class="col-md-12 card card-body text-dark font-weight-bold bg-info">
    <span>Total cases: ${Intl.NumberFormat().format(yesterdayData.cases)}</span>
    </div>
    <div class="col-md-12 card card-body text-dark bg-success font-weight-bold">
    <span>Recovered: ${Intl.NumberFormat().format(yesterdayData.recovered)}</span>
    </div>
    <div class="col-md-12 card card-body text-dark font-weight-bold bg-info">
    <span>Critical cases: ${Intl.NumberFormat().format(yesterdayData.critical)}</span>
    </div>
    <div class="col-md-12 card card-body text-white bg-danger font-weight-bold">
    <span>Total Deaths: ${Intl.NumberFormat().format(yesterdayData.deaths)}</span>
    </div>
    `
}












}